#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <MQTT.h>

// In this project I use the Wemos D1 Mini (but not an official one) (https://aliexpress.com/item/4001291931302.html)
// In your project that could be different so make sure to change the values to fit your project.

// Output PINS //
const int FEEDBACK_LED      = LED_BUILTIN;

// Input PINS //
const int ONE_CUP_BUTTON    = D1; //GPIO5 / D1
const int DOUBLE_CUP_BUTTON = D2; //GPIO4 / D2
const int POWER_ON_BUTTON   = D3; //GPIO0 / D3

// These are different for everyone ofcourse.
// The SSID's are the WiFi networks it needs to connect to + it's passwords.
// Broker is the MQTT broker you wanna use, best is to host your own broker.
// Topic is the channel/topic you want it to subsribe to.
const char* ssid          = "Klote-Netwerk (2.4GHz)";
const char* password      = "Geheimpje";
const char* fallback_ssid = "Klote-Netwerk (5GHz)";
const char* fallback_pass = "Geheimpje";
const char* broker        = "192.168.1.100";
const char* topic         = "/senseo";

ESP8266WiFiMulti WiFiMulti;
MQTTClient client;
WiFiClient net;

void connect_mqtt() {
  Serial.println("\n\nConnecting to MQTT Broker...");

  while (!client.connect("arduino", "try", "try")) {
    digitalWrite(FEEDBACK_LED, LOW);
    delay(500);
    Serial.print(".");
    digitalWrite(FEEDBACK_LED, HIGH);
    delay(500);
  }

  Serial.println("Connected!");
  client.subscribe(topic);
  digitalWrite(FEEDBACK_LED, HIGH);
}

void blink_led(int times) {
  for (int i = 0; i < times; i++) {
    digitalWrite(FEEDBACK_LED, LOW);
    delay(250);
    digitalWrite(FEEDBACK_LED, HIGH);
    delay(250);
  }
}

// This method will run when the board receives "turn_on" command.
void turn_on() {
  blink_led(5);
}

// This method will run when the board receives "one_cup" command.
void one_cup() {
  blink_led(8);
}

// This method will run when the board receives "double_cup" command.
void double_cup() {
  blink_led(11);
}

void on_message(String &topic, String &payload) {
  blink_led(1);
  if (!payload) return;
  if (payload == "turn_on") return turn_on();
  if (payload == "one_cup") return one_cup();
  if (payload == "double_cup") return double_cup();
  return;
}

void setup() {
  pinMode(FEEDBACK_LED, OUTPUT);

  Serial.begin(9600);
  WiFi.mode(WIFI_STA);
  WiFiMulti.addAP(ssid, password);
  WiFiMulti.addAP(fallback_ssid, fallback_pass);

  Serial.print("\nWait for WiFi...");
  while (WiFiMulti.run() != WL_CONNECTED) {
    digitalWrite(FEEDBACK_LED, LOW);
    delay(500);
    Serial.print(".");
    digitalWrite(FEEDBACK_LED, HIGH);
    delay(500);
  }

  digitalWrite(FEEDBACK_LED, HIGH);
  Serial.println("\nWiFi connected");
  Serial.print("IP address: ");
  Serial.print(WiFi.localIP());

  delay(500);
  client.begin(broker, net);
  client.onMessage(on_message);
}

void loop() {
  client.loop();
  delay(10);

  if (!client.connected()) {
    connect_mqtt();
  }
}