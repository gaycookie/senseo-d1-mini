[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/kawaaii/senseo-d1-mini/master?style=for-the-badge)](https://gitlab.com/kawaaii/senseo-d1-mini/-/pipelines)
# "Smart" Senseo Project

## Dependencies
When using [PlatformIO](https://platformio.org) installing the dependencies are very easy!  
Here is the list with all the dependencies this project requires.
- [MQTT.h](https://github.com/256dpi/arduino-mqtt?utm_source=platformio&utm_medium=piohome)

## Requirements
When you wanna use this in your own project there are a couple requirements to be met.  
This project is heavily build with the MQTT socket in mind, so a MQTT Broker is the most important part of the project.  
Beside that a good and stable WiFi connection (prefered 2.4Ghz) is also a good start.

## Make changes
When using [PlatformIO](https://platformio.org) you can simply open it's home screen and click on `Import Arduino Project`  
to import this project and start editing it to your likings.

# Configuration
## Board configuration
Information and examples of the board's configuration can be found [here](https://gitlab.com/kawaaii/senseo-d1-mini/-/wikis/Configuration)

## Home Assistant Example
You can find the examples and information on how to use it, [here](https://gitlab.com/kawaaii/senseo-d1-mini/-/wikis/Home-Assistant)

### More information will follow soon-ish...